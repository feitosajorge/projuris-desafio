package br.com.projuris.interfaces;

public interface FindCharacter {
	char findChar(String word); 
}
