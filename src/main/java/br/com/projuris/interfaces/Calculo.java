package br.com.projuris.interfaces;

import java.util.List;

import br.com.projuris.pojo.CustoCargo;
import br.com.projuris.pojo.CustoDepartamento;
import br.com.projuris.pojo.Funcionario;

public interface Calculo {
	
	public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios);

    public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios);

}
