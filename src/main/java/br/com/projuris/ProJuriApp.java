package br.com.projuris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class ProJuriApp {
    
    public static void main(String[] args) {
    	
    	// Inicia a aplicação
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ProJuriApp.class, args);
    }
}
