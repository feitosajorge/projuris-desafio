package br.com.projuris.pojo;

import java.math.BigDecimal;

public class CustoCargo {
	
	private String cargo; 
    private BigDecimal custo; 

    /**
     * @return the cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * @return the custo
     */
    public BigDecimal getCusto() {
        return custo;
    }

    /**
     * @param custo the custo to set
     */
    public void setCusto(BigDecimal custo) {
        this.custo = custo;
    }

}
