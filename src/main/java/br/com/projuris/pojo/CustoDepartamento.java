package br.com.projuris.pojo;

import java.math.BigDecimal;

public class CustoDepartamento {

	private String departamento;
	private BigDecimal custo;

	/**
	 * @return the departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * @param departamento
	 *            the departamento to set
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * @return the custo
	 */
	public BigDecimal getCusto() {
		return custo;
	}

	/**
	 * @param custo
	 *            the custo to set
	 */
	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}

}
