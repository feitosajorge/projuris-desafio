package br.com.projuris.controller;

import java.util.Arrays;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projuris.interfaces.FindArray;

/**
 * Classe responsável por configurar todas as execuções via REST do Sprint Boot
 */
@RestController
@RequestMapping("/findmyarray")
public class FindMyArray implements FindArray{

	@RequestMapping(method = RequestMethod.GET, produces = "application/json", path = "/{array}/{subarray}")
	public int getArray(@PathVariable("array") String arrayString, @PathVariable("subarray") String subarrayString) {
		String[] spltArray = arrayString.split(",");
		String[] spltSubArray = subarrayString.split(",");
		int[] array = new int[spltArray.length];
        int[] subArray = new int[spltSubArray.length];
        
        for(int i = 0; i < spltArray.length; i++) {
        	array[i] = Integer.parseInt(spltArray[i]);
        }
        
        for(int i = 0; i < spltSubArray.length; i++) {
        	subArray[i] = Integer.parseInt(spltSubArray[i]);
        }
        
		return findArray(array, subArray);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public int getArrayDefault() {
		int[] array = {4,9,3,7,8,3,7,1};
        int[] subArray = {3,7};
		return findArray(array, subArray);
	}

	@Override
	public int findArray(int[] array, int[] subArray) {
		int limitPosition = array.length;
        int subArraySize = subArray.length;
        Integer lastOcurrence = null;
        
        for(int i = 0; i < limitPosition; i++) {
            
            if((i + (subArraySize -1)) < limitPosition &&
                    Arrays.equals(Arrays.copyOfRange(array, i, i + (subArraySize)), subArray) ) {
                lastOcurrence = i;
            }
        }
        
        if(lastOcurrence != null) {
            return lastOcurrence;
        }
        
        return -1;
	}
	
}
