package br.com.projuris.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projuris.interfaces.Calculo;
import br.com.projuris.pojo.CustoCargo;
import br.com.projuris.pojo.CustoDepartamento;
import br.com.projuris.pojo.Funcionario;

/**
 * Classe responsável por configurar todas as execuções via REST do Sprint Boot
 */
@RestController
@RequestMapping("/mycalculo")
public class MyCalculo implements Calculo {
	
	List<Funcionario> listaFuncionario = new ArrayList<>();
	
	public MyCalculo() {
		Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
        Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
        Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
        Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
        Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
        Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
        Funcionario funcionario7 = new Funcionario("Estagiário", "Jurídico", new BigDecimal(700.4));
        Funcionario funcionario8 = new Funcionario("Assistente", "Jurídico", new BigDecimal(1800.90));
        Funcionario funcionario9 = new Funcionario("Gerente", "Jurídico", new BigDecimal(9500.50));
        Funcionario funcionario10 = new Funcionario("Diretor", "Jurídico", new BigDecimal(13000.0));
        
        listaFuncionario.add(funcionario1);
        listaFuncionario.add(funcionario2);
        listaFuncionario.add(funcionario3);
        listaFuncionario.add(funcionario4);
        listaFuncionario.add(funcionario5);
        listaFuncionario.add(funcionario6);
        listaFuncionario.add(funcionario7);
        listaFuncionario.add(funcionario8);
        listaFuncionario.add(funcionario9);
        listaFuncionario.add(funcionario10);
	}
		
	@RequestMapping(value = "/cargo", method = RequestMethod.GET, produces = "application/json")
	public List<CustoCargo> getPorCargo() {
		return custoPorCargo(listaFuncionario);
	}
	
	@RequestMapping(value = "/departamento", method = RequestMethod.GET, produces = "application/json")
	public List<CustoDepartamento> getPorDepartamento() {
		return custoPorDepartamento(listaFuncionario);
	}
	
	@RequestMapping(value = "/cargo", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public List<CustoCargo> putPorCargo(@RequestBody List<Map<String, Object>> funcionarios) {
		
		List<Funcionario> listaCargoFuncionario = new ArrayList<>();
		for(Map<String, Object> funcionario : funcionarios) {
			listaCargoFuncionario.add(new Funcionario(funcionario.get("cargo").toString(), funcionario.get("departamento").toString(), new BigDecimal(Double.parseDouble(funcionario.get("salario").toString()))));
		}
		
        return custoPorCargo(listaCargoFuncionario);
    }
	
	@RequestMapping(value = "/departamento", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public List<CustoDepartamento> putPorDepartamento(@RequestBody List<Map<String, Object>> funcionarios) {
		
		List<Funcionario> listaDepartamentoFuncionario = new ArrayList<>();
		for(Map<String, Object> funcionario : funcionarios) {
			listaDepartamentoFuncionario.add(new Funcionario(funcionario.get("cargo").toString(), funcionario.get("departamento").toString(), new BigDecimal(Double.parseDouble(funcionario.get("salario").toString()))));
		}
		
        return custoPorDepartamento(listaDepartamentoFuncionario);
    }

	@Override
    public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios) {

        List<CustoCargo> custoTotalPorCargo = new ArrayList<>();

        for (int i = 0; i < funcionarios.size(); i++) {

            CustoCargo custoCargo = new CustoCargo();
            custoCargo.setCargo(funcionarios.get(i).getCargo());
            custoCargo.setCusto(funcionarios.get(i).getSalario());

            if (custoTotalPorCargo.isEmpty()) {
                custoTotalPorCargo.add(custoCargo);
            } else {

                for (int j = 0; j < custoTotalPorCargo.size(); j++) {

                    if (custoTotalPorCargo.get(j) != null && custoTotalPorCargo.get(j).getCargo().equals(custoCargo.getCargo())) {
                        custoTotalPorCargo.get(j).setCusto(custoCargo.getCusto().add(custoTotalPorCargo.get(j).getCusto()));
                        break;
                    }

                    if (j == custoTotalPorCargo.size() - 1) {
                        custoTotalPorCargo.add(custoCargo);
                        break;
                    }

                }
            }
        }
        
        return custoTotalPorCargo;

    }

    @Override
    public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios) {
        List<CustoDepartamento> custoTotalPorDepartamento = new ArrayList<>();

        for (int i = 0; i < funcionarios.size(); i++) {

            CustoDepartamento custoDepartamento = new CustoDepartamento();
            custoDepartamento.setDepartamento(funcionarios.get(i).getDepartamento());
            custoDepartamento.setCusto(funcionarios.get(i).getSalario());

            if (custoTotalPorDepartamento.isEmpty()) {
                custoTotalPorDepartamento.add(custoDepartamento);
            } else {

                for (int j = 0; j < custoTotalPorDepartamento.size(); j++) {

                    if (custoTotalPorDepartamento.get(j) != null && custoTotalPorDepartamento.get(j).getDepartamento().equals(custoDepartamento.getDepartamento())) {
                        custoTotalPorDepartamento.get(j).setCusto(custoDepartamento.getCusto().add(custoTotalPorDepartamento.get(j).getCusto()));
                        break;
                    }

                    if (j == custoTotalPorDepartamento.size() - 1) {
                        custoTotalPorDepartamento.add(custoDepartamento);
                        break;
                    }

                }
            }
        }
        
        return custoTotalPorDepartamento;
    }

}
