package br.com.projuris.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projuris.interfaces.FindCharacter;

/**
 * Classe responsável por configurar todas as execuções via REST do Sprint Boot
 */
@RestController
@RequestMapping("/myfindchar")
public class MyFindChar implements FindCharacter {

	@RequestMapping(method = RequestMethod.GET, produces = "application/json", value ="/{word}")
	public Character getChar(@PathVariable("word") String word) {
		MyFindChar myFindChar = new MyFindChar();
		if(!word.isEmpty()) {
			return myFindChar.findChar(word);
		} 
		return null;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public Character getCharDefault() {
		MyFindChar myFindChar = new MyFindChar();
		return myFindChar.findChar("reembolsar");
	}

	@Override
	public char findChar(String word) {
		Map<Character, Integer> ocurrences = new LinkedHashMap<>();

		for (int i = 0; i < word.length(); i++) {
			int count = ocurrences.containsKey(word.charAt(i)) ? ocurrences.get(word.charAt(i)) : 0;
			ocurrences.put(word.charAt(i), count + 1);
		}

		Character matchingKey = ocurrences.entrySet().stream().filter(entry -> entry.getValue().equals(1))
				.map(Map.Entry::getKey).findFirst().orElse(null);

		return matchingKey != null ? matchingKey : Character.MIN_VALUE;
	}

}
