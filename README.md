## *Tutorial de Instalação*


### *Tecnologias utilizadas e seus links para download*
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* Spring Boot
* [Maven](https://maven.apache.org/download.cgi)

### *Construindo o projeto*
1. Tenha certeza que o Java 1.8 e o Maven estão instalados e funcionando apropriadamente em sua máquina local
4. Faça o clone do projeto para a sua máquina local: ```git clone https://feitosajorge@bitbucket.org/feitosajorge/projuris-desafio.git``` e vá para a branch ```master```
5. Navegue até a pasta raiz do projeto ```projuris-desafio```
9. Tenha certeza que contém o Maven instalado em sua máquina ou em sua IDE, caso queira usar um para rodar o código
10. Execute o comando na pasta raiz do projeto via CMD: ```mvn clean package```
   - Pode executar o maven usando sua IDE, ao invés

### *Como executar o projeto*
* Execute o comando abaixo na raiz do projeto ```projuris-desafio```:
```mvn spring-boot:run```
* Acesse sua porta local com o /projuris/ como base, exemplo:
```http://127.0.0.1:8080/projuris/```

* Exemplos de execução GET:
	- ```http://localhost:8080/projuris/findmyarray/1,2,3,4,5/4,5```
	- ```http://localhost:8080/projuris/findmyarray/```
	- ```http://localhost:8080/projuris/myfindchar/```
	- ```http://localhost:8080/projuris/myfindchar/palavra```
	- ```http://localhost:8080/projuris/mycalculo/cargo```
	- ```http://localhost:8080/projuris/mycalculo/departamento```
	
* Exemplos de execução POST:	

URL: ```http://localhost:8080/projuris/mycalculo/departamento``` ou ```http://localhost:8080/projuris/mycalculo/cargo```

Header: ```Content-Type: application/json```

Body:

```
[
  
  {
    "cargo": "Desenvolvedor",
    "departamento": "Pesquisa",
    "salario": 100.30
  },
  {
    "cargo": "Desenvolvedor",
    "departamento": "Pesquisa",
    "salario": 100.30
  },
  {
    "cargo": "Gerente",
    "departamento": "Saúde",
    "salario": 127.30
  }

]
```